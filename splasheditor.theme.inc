<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 5/05/14
 * Time: 3:51 PM
 */
/**
 * Prepares variables for list of available node type templates.
 *
 * Default template: node-add-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of content types.
 *
 */
function template_preprocess_droplet_add_list(&$variables) {
    foreach ($variables['plugins'] as $type) {
        $variables['plugins'][$type['type']]['add_link'] = l($type['name'], 'admin/structure/splash/add/' . $type['id']);
    }
}
