<?php

/**
 * Implements hook_permission().
 */
function splasheditor_permission() {
    return array(
        'use splash editor' => array(
            'title' => t('Use Splash Editor'),
        ),
    );
}

/**
 * Load Droplet
 *
 * @param $id
 * @return \Drupal\Core\Entity\EntityInterface
 */
function droplet_load($id) {
    return entity_load('droplet', $id);
}

/**
 * Implements hook_library_info_alter().
 */
function splasheditor_library_info_alter(&$libraries, $module) {
    if ($module == 'splasheditor') {
        $droplets = entity_load_multiple('droplet');

        foreach ($droplets as $droplet) {
            if ($droplet->getPlugin() instanceof \Drupal\splasheditor\DropletFileInterface) {

                if ($info = $droplet->getPlugin()->insertLibraryInfo()) {
                    $libraries += $info;
                }
            }
        }
    }
}

/**
 * Implements hook_page_build().
 */
function splasheditor_page_build(&$page) {
    $droplets = entity_load_multiple('droplet');

    foreach ($droplets as $droplet) {
        if (!$droplet->access('view')) {
            continue;
        }
        if ($return = $droplet->getPlugin()->event('pageBuild', $page)) {
            $page = $return;
        }
    }
}

/**
 * Implements hook_html_head_alter().
 */
function splasheditor_html_head_alter(array &$elements) {
    $droplets = entity_load_multiple('droplet');

    foreach ($droplets as $droplet) {
        if (!$droplet->access('view')) {
            continue;
        }
        if ($return = $droplet->getPlugin()->event('htmlHead', $elements)) {
            $elements[$droplet->id()] = $return;
        }
    }
}

/**
 * Implements hook_theme().
 */
function splasheditor_theme() {
    return array(
        'droplet_add_list' => array(
            'variables' => array('plugins' => array()),
            'file' => 'splasheditor.theme.inc',
            'template' => 'droplet-add-list',
        ),
    );
}