/**
 * Created by josh.waihi on 3/05/14.
 */
(function (Drupal, CodeMirror, $) {

    "use strict";
    Drupal.behaviors.SplashEditor = {
        attach: function () {
            $('#edit-settings-code').once(function () {
                CodeMirror.fromTextArea(this, {
                    lineNumbers: true,
                    mode: "css",
                    theme: "monokai",
                    keyMap: "sublime"
                });
            })
        }

    }


})(Drupal, CodeMirror, jQuery);