<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 3:30 PM
 */

namespace Drupal\splasheditor\Entity;

use Drupal\splasheditor\DropletPluginBag;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\EntityWithPluginBagInterface;



/**
 * Defines a Block configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "droplet",
 *   label = @Translation("Droplet"),
 *   controllers = {
 *     "access" = "Drupal\splasheditor\DropletAccessController",
 *     "list_builder" = "Drupal\splasheditor\DropletListBuilder",
 *     "form" = {
 *       "add" = "Drupal\splasheditor\DropletFormController",
 *       "edit" = "Drupal\splasheditor\DropletFormController"
 *     },
 *     "list_builder" = "Drupal\splasheditor\DropletListBuilder",
 *   },
 *   admin_permission = "use splash editor",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "plugin" = "plugin"
 *   },
 *   links = {
 *     "edit-form" = "splasheditor.edit_droplet",
 *     "enable" = "splasheditor.enable_droplet",
 *     "disable" = "splasheditor.disable_droplet"
 *   }
 * )
 */
class Droplet extends ConfigEntityBase implements EntityWithPluginBagInterface {

    /**
     * The Entity label.
     *
     * @var string
     */
    public $label;

    /**
     * The Entity machine name.
     *
     * @var string
     */
    public $id;

    /**
     * The plugin instance settings.
     *
     * @var array
     */
    public $settings = array();

    /**
     * The plugin instance ID.
     *
     * @var string
     */
    public $plugin;

    /**
     * The plugin bag that holds the block plugin for this entity.
     *
     * @var \Drupal\block\BlockPluginBag
     */
    protected $pluginBag;

    /**
     * {@inheritdoc}
     */
    protected $pluginConfigKey = 'settings';

    /**
     * The visibility settings.
     *
     * @var array
     */
    public $visibility;

    /**
     * {@inheritdoc}
     */
    public function getPlugin() {
        return $this->getPluginBag()->get($this->plugin);
    }

    /**
     * Returns the plugin bag used by this entity.
     *
     * @return \Drupal\Component\Plugin\PluginBag
     */
    public function getPluginBag()
    {
        if (!$this->pluginBag) {
            $this->pluginBag = new DropletPluginBag(\Drupal::service('plugin.manager.splasheditor'), $this->plugin, $this->get('settings'), $this->id());
        }
        return $this->pluginBag;
    }
}