<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 12:00 PM
 */

namespace Drupal\splasheditor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\splasheditor\Entity\Droplet;
use Symfony\Component\DependencyInjection\ContainerInterface;


class SplashEditorController extends ControllerBase {

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container) {
        return new static(
            $container->get('module_handler')
        );
    }

    /**
     *  Enable/Disable Droplet
     *
     */
    public function operation(Droplet $droplet, $op) {
        $droplet->$op()->save();
        drupal_set_message($this->t('Droplet has been @status.', array(
            '@status' => $droplet->status() ? 'enabled' : 'disabled',
        )));
        return $this->redirect('splasheditor.admin_display');
    }

    /**
     *  Enable/Disable Droplet
     *
     */
    public function delete(Droplet $droplet) {
        $droplet->delete();
        drupal_set_message($this->t('Droplet has been deleted.'));
        return $this->redirect('splasheditor.admin_display');
    }

    /**
     * Add a Droplet.
     *
     * @return array
     */
    public function addDroplet() {
        $plugin_manager = \Drupal::service('plugin.manager.splasheditor');
        return array(
          '#theme' => 'droplet_add_list',
          '#plugins' => $plugin_manager->getDefinitions()
        );
    }

} 