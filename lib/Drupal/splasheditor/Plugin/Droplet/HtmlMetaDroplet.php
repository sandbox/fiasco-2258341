<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 1:20 PM
 */

namespace Drupal\splasheditor\Plugin\Droplet;

use Drupal\splasheditor\DropletBase;
use mixed;
use string;

/**
 * Provides a 'HTML Meta tag' plugin to splash editor.
 *
 * @Droplet(
 *   id   = "html_meta",
 *   type = "html_meta",
 *   name = @Translation("HTML Meta Tag"),
 *   description = @Translation("Embed additional HTML Meta tags in the HEAD section of the HTML document of Drupal.")
 * )
 */
class HtmlMetaDroplet extends DropletBase {

    public function buildConfigurationForm(array $form, array &$form_state) {
        $config = $this->configuration + array(
            'name' => '',
            'content' => '',
        );
        $form['name'] = [
            '#type' => 'textfield',
            '#default_value' => $config['name'],
            '#title' => 'Name',
            '#description' => 'The name attribute value for the meta tag.',
        ];

        $form['content'] = [
            '#type' => 'textfield',
            '#default_value' => $config['content'],
            '#title' => 'Content',
            '#description' => 'The content attribute value for the meta tag.',
        ];
        return $form;
    }

    /**
     * Insert Javascript in page build.
     */
    public function htmlHead() {
        return array(
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => array(
                'name' => $this->configuration['name'],
                'content' => $this->configuration['content'],
            ),
        );
    }

}