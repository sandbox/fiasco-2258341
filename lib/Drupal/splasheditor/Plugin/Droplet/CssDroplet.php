<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 1:20 PM
 */

namespace Drupal\splasheditor\Plugin\Droplet;

use Drupal\splasheditor\DropletFile;
use mixed;
use string;

/**
 * Provides a 'HTML Meta tag' plugin to splash editor.
 *
 * @Droplet(
 *   id   = "css",
 *   type = "css",
 *   name = @Translation("CSS"),
 *   description = @Translation("Enable small style tweaks by embedding CSS into the page to alter appearance.")
 * )
 */
class CssDroplet extends DropletFile {

    public function buildConfigurationForm(array $form, array &$form_state) {
        $config = $this->configuration + array(
            'code' => '',
        );
        $form['code'] = [
            '#type' => 'textarea',
            '#default_value' => $config['code'],
            '#title' => 'CSS Code',
            '#description' => 'The CSS code to add to a page.',
        ];
        $form['code']['#attached']['library'][] = 'splasheditor/splasheditor.codemirror.css';
        return $form;
    }

    /**
     * Return a library info array or FALSE.
     *
     * @return mixed
     */
    public function insertLibraryInfo()
    {
        return ['droplet.' . $this->getFilename() => [
            'version' => '1.x',
            'css' => [
                'theme' => [
                    $this->getFileURI() => [],
                ]
            ],
        ]];
    }

    /**
     * Insert Javascript in page build.
     */
    public function pageBuild($page) {
        $page['#attached']['library'][] = 'splasheditor/droplet.' . $this->getFilename();
        return $page;
    }
}