<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 1:20 PM
 */

namespace Drupal\splasheditor\Plugin\Droplet;

use Drupal\splasheditor\DropletFile;
use mixed;
use string;

/**
 * Provides a 'Javascript' plugin to splash editor.
 *
 * @Droplet(
 *   id   = "js",
 *   type = "js",
 *   name = @Translation("Javascript"),
 *   description = @Translation("Embed Javascript scripts in Drupal to provide small customizations and tweaks.")
 * )
 */
class JavascriptDroplet extends DropletFile {

    public function buildConfigurationForm(array $form, array &$form_state) {
        $config = $this->configuration + array(
                'code' => '',
                'scope' => 'header',
        );
        $form['code'] = [
            '#type' => 'textarea',
            '#default_value' => $config['code'],
            '#title' => t('Javascript Code'),
            '#description' => 'The Javascript code to add to a page.',
        ];
        $form['code']['#attached']['library'][] = 'splasheditor/splasheditor.codemirror.js';

        $form['scope'] = [
            '#type' => 'radios',
            '#title' => t('Scope'),
            '#options' => array(
              'header' => t('Head element'),
              'footer' => t('Bottom of page')
            ),
            '#default_value' => $config['scope'],
            '#description' => t('Choose where to include the Javascript.'),
        ];
        return $form;
    }

    /**
     * Return a library info array or FALSE.
     *
     * @return mixed
     */
    public function insertLibraryInfo()
    {
        return ['droplet.' . $this->getFilename() => [
            'version' => '1.x',
            'js' => [
                $this->getFileURI() => [],
            ],
        ]];
    }

    /**
     * Insert Javascript in page build.
     */
    public function pageBuild($page) {
        $page['#attached']['library'][] = 'splasheditor/droplet.' . $this->getFilename();
        return $page;
    }
}