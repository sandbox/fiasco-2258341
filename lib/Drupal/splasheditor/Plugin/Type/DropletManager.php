<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 1:37 PM
 */

namespace Drupal\splasheditor\Plugin\Type;


use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\StringTranslation\TranslationInterface;

class DropletManager extends DefaultPluginManager {
    public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager, ModuleHandlerInterface $module_handler, TranslationInterface $translation_manager) {
        parent::__construct('Plugin/Droplet', $namespaces, $module_handler, 'Drupal\splasheditor\Annotation\Droplet');

        $this->alterInfo('droplet');
        $this->setCacheBackend($cache_backend, $language_manager, 'splasheditor_plugins');
    }
} 