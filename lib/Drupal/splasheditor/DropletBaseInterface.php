<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 12:56 PM
 */

namespace Drupal\splasheditor;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountInterface;


interface DropletBaseInterface extends PluginFormInterface {

    /**
     * Respond to an emitted event.
     *
     * @param $method
     * @return mixed
     */
    public function event($method);

    /**
     * Determine if the account may use the plugin.
     *
     * @param AccountInterface $account
     * @return boolean
     */
    public function access(AccountInterface $account);

}