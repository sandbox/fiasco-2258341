<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 5:01 PM
 */

namespace Drupal\splasheditor;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\String;
use Drupal\Core\Plugin\DefaultSinglePluginBag;

class DropletPluginBag extends DefaultSinglePluginBag {
    /**
     * {@inheritdoc}
     */
    protected function initializePlugin($instance_id) {
        if (!$instance_id && isset($this->instanceId)) {
            $instance_id = $this->instanceId;
        }

        if (!$instance_id) {
            throw new PluginException(String::format("The droplet did not specify a plugin."));
        }

        try {
            parent::initializePlugin($instance_id);
        }
        catch (PluginException $e) {
            throw $e;
        }
    }
} 