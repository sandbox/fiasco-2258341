<?php
/**
 * Created by PhpStorm.
 * User: josh.waihi
 * Date: 24/04/14
 * Time: 4:01 PM
 */

namespace Drupal\splasheditor;


use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityForm;

class DropletFormController extends EntityForm {
    public function form(array $form, array &$form_state) {
        $form = parent::form($form, $form_state);

        if ($this->entity->isNew() && $plugin_id = $this->getRequest()->get('plugin_id')) {
            $this->entity->set('plugin', $plugin_id);
        }

        $form['plugin'] = array(
            '#type' => 'hidden',
            '#value' => $this->entity->get('plugin'),
        );
        $form['label'] = array(
            '#title' => $this->t('Droplet name'),
            '#type' => 'textfield',
            '#default_value' => $this->entity->get('label'),
            '#required' => TRUE,
            '#size' => 30,
        );
        $form['id'] = array(
            '#type' => 'machine_name',
            '#default_value' => $this->entity->id(),
            '#disabled' => !$this->entity->isNew(),
            '#machine_name' => array(
                'source' => array('label'),
                'exists' => 'droplet_load',
            ),
        );

        $form['settings'] = $this->entity->getPlugin()->buildConfigurationForm(array(), $form_state);
        $form['#tree'] = TRUE;

        // Visibility settings.
        $form['visibility'] = array(
            '#type' => 'vertical_tabs',
            '#title' => $this->t('Visibility settings'),
            '#attached' => array(
                'library' => array(
                    'block/drupal.block',
                ),
            ),
            '#tree' => TRUE,
            '#weight' => 10,
            '#parents' => array('visibility'),
        );

        // Per-path visibility.
        $form['visibility']['path'] = array(
            '#type' => 'details',
            '#title' => $this->t('Pages'),
            '#group' => 'visibility',
            '#weight' => 0,
        );

        // @todo remove this access check and inject it in some other way. In fact
        //   this entire visibility settings section probably needs a separate user
        //   interface in the near future.
        $visibility = $this->entity->get('visibility');
        $options = array(
            BLOCK_VISIBILITY_NOTLISTED => $this->t('All pages except those listed'),
            BLOCK_VISIBILITY_LISTED => $this->t('Only the listed pages'),
        );
        $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %user for the current user's page and %user-wildcard for every user page. %front is the front page.", array('%user' => 'user', '%user-wildcard' => 'user/*', '%front' => '<front>'));

        $form['visibility']['path']['visibility'] = array(
            '#type' => 'radios',
            '#title' => $this->t('Show block on specific pages'),
            '#options' => $options,
            '#default_value' => !empty($visibility['path']['visibility']) ? $visibility['path']['visibility'] : BLOCK_VISIBILITY_NOTLISTED,
        );
        $form['visibility']['path']['pages'] = array(
            '#type' => 'textarea',
            '#title' => '<span class="visually-hidden">' . $this->t('Pages') . '</span>',
            '#default_value' => !empty($visibility['path']['pages']) ? $visibility['path']['pages'] : '',
            '#description' => $description,
        );

        // Per-role visibility.
        $role_options = array_map('check_plain', user_role_names());
        $form['visibility']['role'] = array(
            '#type' => 'details',
            '#title' => $this->t('Roles'),
            '#group' => 'visibility',
            '#weight' => 10,
        );
        $form['visibility']['role']['roles'] = array(
            '#type' => 'checkboxes',
            '#title' => $this->t('Show block for specific roles'),
            '#default_value' => !empty($visibility['role']['roles']) ? $visibility['role']['roles'] : array(),
            '#options' => $role_options,
            '#description' => $this->t('Use this only for the selected role(s). If you select no roles, the will be rendered to all users.'),
        );

        $theme_options = array();
        foreach (list_themes() as $theme_name => $theme_info) {
            if (!empty($theme_info->status)) {
                $theme_options[$theme_name] = $theme_info->info['name'];
            }
        }
        $form['visibility']['themes'] = array(
            '#type' => 'details',
            '#title' => $this->t('Themes'),
            '#group' => 'visibility',
            '#weight' => 10,
        );
        $form['visibility']['themes']['theme'] = array(
            '#type' => 'checkboxes',
            '#options' => $theme_options,
            '#title' => t('Use on select themes'),
    //        '#default_value' => $theme,
            '#ajax' => array(
                'callback' => array($this, 'themeSwitch'),
                'wrapper' => 'edit-block-region-wrapper',
            ),
        );
        return $form;
    }

    public function save(array $form, array &$form_state) {
        parent::submit($form, $form_state);

        $entity = $this->entity;
        // The Block Entity form puts all block plugin form elements in the
        // settings form element, so just pass that to the block for submission.
        // @todo Find a way to avoid this manipulation.
        $settings = array(
            'values' => &$form_state['values']['settings'],
            'errors' => $form_state['errors'],
        );

        // Call the plugin submit handler.
        $entity->getPlugin()->submitConfigurationForm($form, $settings);

        // Save the settings of the plugin.
        $status = $entity->save();

        // Now we need to rebuild the discovery.library cache.
        $cache_backends = Cache::getBins();
        $cache = $cache_backends['discovery'];
        $cache->deleteTags(array(
            'extension' => 'splasheditor',
        ));

        if ($status == SAVED_UPDATED) {
            drupal_set_message(t('Droplet updated.'));
        }
        elseif ($status == SAVED_NEW) {
            drupal_set_message(t('New Droplet has been created'));
            // watchdog('node', 'Added content type %name.', $t_args, WATCHDOG_NOTICE, l(t('View'), 'admin/structure/types'));
        }
        $form_state['redirect_route']['route_name'] = 'splasheditor.admin_display';
    }

} 