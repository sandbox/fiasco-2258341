<?php

namespace Drupal\splasheditor;

interface DropletFileInterface extends DropletBaseInterface {

    public function getFilename();

    public function generateFilename();

    public function getFileURI();

    /**
     * @return mixed
     */
    public function writeFile();

    /**
     * Return a library info array or FALSE.
     *
     * @return mixed
     */
    public function insertLibraryInfo();
}