<?php

/**
 * @file
 * Contains \Drupal\block\Annotation\Block.
 */

namespace Drupal\splasheditor\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Block annotation object.
 *
 * @Annotation
 */
class Droplet extends Plugin {

  /**
   * The plugin type.
   *
   * @var string
   */
  public $type;

  /**
   * The human readable name of the droplet.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name = '';

  /**
   * Description of the plugin.
   *
   * @var string
   */
  public $description = '';
}
