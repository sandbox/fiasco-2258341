<?php

namespace Drupal\splasheditor;

class DropletFile extends DropletBase implements DropletFileInterface {

    protected $directory = 'public://splasheditor';

    public function generateFilename()
    {
        return $this->configuration['filename'] = md5($this->configuration['code']) . '.' . $this->getPluginId();
    }

    /**
     * Write code to file.
     *
     * @throws \Exception
     * @return mixed
     */
    public function writeFile()
    {
        $directory = implode('/', [$this->directory, $this->getPluginId()]);
        if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY)) {
            return file_put_contents($this->getFileURI(), $this->configuration['code']);
        }
        throw new \Exception(t('Was unable to to create %directory', array(
            '%directory' => $directory,
        )));
    }

    public function getFilename()
    {
        return isset($this->configuration['filename']) ? $this->configuration['filename'] : $this->generateFilename();
    }

    /**
     * Form submission handler.
     *
     * To properly store submitted form values store them in $this->configuration.
     * @code
     *   $this->configuration['some_value'] = $form_state['values']['some_value'];
     * @endcode
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param array $form_state
     *   An associative array containing the current state of the form.
     */
    public function submitConfigurationForm(array &$form, array &$form_state)
    {
        parent::submitConfigurationForm($form, $form_state);

        // Write a new file in place if the code has changed.
        $original_filename = $this->getFilename();
        $this->configuration['code'] = $form_state['values']['code'];

        if ($original_filename != $this->generateFilename()) {
            $this->writeFile();
            $this->configuration['filename'] = $this->generateFilename();
        }
    }

    public function getFileURI()
    {
        return implode('/', array($this->directory, $this->getPluginId(), $this->getFilename()));
    }

    /**
     * Return a library info array or FALSE.
     *
     * @return mixed
     */
    public function insertLibraryInfo()
    {
        return FALSE;
    }
}