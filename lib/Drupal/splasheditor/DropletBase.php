<?php

namespace Drupal\splasheditor;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use mixed;
use string;

class DropletBase extends PluginBase implements DropletBaseInterface {

    /**
     * Form constructor.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param array $form_state
     *   An associative array containing the current state of the form.
     *
     * @return array
     *   The form structure.
     */
    public function buildConfigurationForm(array $form, array &$form_state)
    {
        // TODO: Implement buildConfigurationForm() method.
        return $form;
    }

    /**
     * Form validation handler.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param array $form_state
     *   An associative array containing the current state of the form.
     */
    public function validateConfigurationForm(array &$form, array &$form_state)
    {
        // TODO: Implement validateConfigurationForm() method.
    }

    /**
     * Form submission handler.
     *
     * To properly store submitted form values store them in $this->configuration.
     * @code
     *   $this->configuration['some_value'] = $form_state['values']['some_value'];
     * @endcode
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param array $form_state
     *   An associative array containing the current state of the form.
     */
    public function submitConfigurationForm(array &$form, array &$form_state)
    {
        // TODO: Implement submitConfigurationForm() method.
    }

    /**
     * Respond to an emitted event.
     *
     * @param $method
     * @return boolean
     */
    public function event($method)
    {
        $args = func_get_args();
        array_shift($args);

        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $args);
        }
        return FALSE;
    }

    public function access(AccountInterface $account) {
        return TRUE;
    }
}